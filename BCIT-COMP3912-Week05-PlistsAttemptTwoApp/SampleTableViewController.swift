//
//  SampleTableViewController.swift
//  BCIT-COMP3912-Week05-PlistsAttemptTwoApp
//
//  Created by Massimo Savino on 2016-10-23.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit

class SampleTableViewController: UITableViewController {
    
    // MARK: Properties
    
    var isColorInverted = true
    
    var color1 = UIColor(red: 0.3, green: 0.3, blue: 0.0, alpha: 1.0)
    let color2 = UIColor(white: 0.9, alpha: 1.0)
    
    var pies = ["Pizza", "Chicken Pot", "Shepherd's"]

    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        readPropertyList()
        
    }

    
    // MARK: Helper functions
    
    func readPropertyList() {
        var format = PropertyListSerialization.PropertyListFormat.xml
        
        var plistData: [String: AnyObject] = [:]
        
        let plistPath: String? = Bundle.main.path(forResource: "data", ofType: "plist")!
        
        let plistXML = FileManager.default.contents(atPath: plistPath!)!
        
        do {
            plistData = try PropertyListSerialization.propertyList(from: plistXML, options: .mutableContainersAndLeaves, format: &format) as! [String: AnyObject]
            isColorInverted = plistData["Inverse Color"] as! Bool
            
            guard let red = plistData["Red"] as? CGFloat else { return }
            guard let green = plistData["Green"] as? CGFloat else { return }
            guard let blue =  plistData["Blue"] as? CGFloat else { return }
            
            color1 = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            pies = plistData["Pies"] as! [String]
            
        } catch {
            print("Error reading plist: \(error), format: \(format)")
        }
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pies.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SampleTableViewCell
        let row = indexPath.row
        
        cell.pieLabel?.text = pies[row]
        
        if isColorInverted {    // inverting colors by flag status
            
            cell.backgroundColor = color2
            cell.pieLabel?.textColor = color1
            
        } else {
            cell.backgroundColor = color1
            cell.pieLabel?.textColor = color2
        }
        
        return cell
    }


    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Pies"
    }
}
